=====
Login
=====

Login is a simple administrable Django app to login HTML5 sexy animated login screen.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "login" to your INSTALLED_APPS setting like this::

  INSTALLED_APPS = [
    …
    'django.contrib.auth',
    'django.contrib.contenttypes',
    …
    'static_precompiler',
    'login',
  ]

2. Include the login URLconf in your project urls.py like this::

  from django.urls import include, path, re_path
  from django.contrib import admin
  
  urlpatterns = [
    re_path(r'log', include('login.urls')),
    re_path(r'admin', admin.site.urls),
    path('', include('home.urls'), name='index'),
  ]

3. Include template login to path templates like this::

  TEMPLATES = [
    …
    'DIRS': [os.path.join(BASE_DIR, 'templates')],
    …
  ]

  STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
  ]

  #LESS Gesture
  STATIC_PRECOMPILER_ROOT = os.path.join(BASE_DIR, 'css_compiled')
  
  STATIC_PRECOMPILER_COMPILERS = (
    ('static_precompiler.compilers.LESS', {
      'executable': '/usr/bin/lessc',
      'global_vars': {'link-color': 'red'},
    }),
  )

  STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #LESS Gesture with django compressor
    'static_precompiler.finders.StaticPrecompilerFinder',
  ]

4. Chose the home directory like this::

  # Redirect home site page after login
  LOGIN_REDIRECT_URL = 'index'

5. Configure require login to site like this::
  #Modify home/views.py
  from django.shortcuts import render
  from django.contrib.auth.decorators import login_required
  
  # Create your views here.
  @login_required(login_url='/login/')
  def home(request):
    return render(request, 'portal/index.html')


6. Create superuser admin if not like this::
  python manage.py createsuperuser


7. Create the login models like this::
  python manage.py migrate

8. Start the development server like this:
  python manage.py runserver

9. Visit http://127.0.0.1:8000/admin/ to modify login screen features.
