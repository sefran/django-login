#file django-login/setup.py 
import os
from setuptools import find_packages, setUp

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
  README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
  name='django-login',
  version='0.5',
  packages=find_packages(),
  include_package_data=True,
  licence='GPLv3+'
  description'A administrable Django HTML5 sexy login screen',
  long_description=README,
  url='https://framagit.org/sefran/django-login',
  author='Franc SERRES',
  author_email='sefran007@gmail.com',
  classifiers=[
    'Environment :: Web Environment',
    'Framework :: Django',
    'Framework :: Django :: 2.1',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'TOPIC :: Internet :: WWW/HTTP',
    'TOPIC :: Internet :: WWW/HTTP :: Dynamic Content',
  ],
)
