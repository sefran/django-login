#file gocea/forms.py

#import floppyforms as forms
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

class Button(forms.Widget):
  """Base class for all <button> widgets."""
  button_type = None
  """
  button_type (string) : value is 'button', 'reset' or 'submit'
  """
  template_name = 'login/button.html'

  def __init__(self, attrs=None):
    """
      Constructor
      
      Args:
          attrs (list)
    """
    if attrs is not None:
        attrs = attrs.copy()
        # Remove attribut type of button from list attrs and replace into Button object button_type
        self.button_type = attrs.pop('type', self.button_type)
    super().__init__(attrs)

  def get_context(self, name, value, attrs):
    context = super().get_context(name, value, attrs)
    context['widget']['type'] = self.button_type
    return context

class HtmlButton(Button):
  """Widget button HTML 5."""
  button_type = 'button'

class ResetButton(Button):
  """Widget button reset."""
  button_type = 'reset'

class SubmitButton(Button):
  """Widget button submit."""
  button_type = 'submit'

class ButtonField(forms.Field):
  """Button field"""
  def __init__(self, *args, **kwargs):
    """
      Constructor
      
      Args:
        *args (list): 
        **kwargs (dict): pass a keyworded.
    """
    super(ButtonField, self).__init__(**kwargs)
      
  def to_python(self, value):
    """Return a sting"""
    value = str(value).strip()
    return value
  
  def widget_attrs(self, widget):
    """Return attrs"""
    attrs = super().widget_attrs(widget)
    return attrs

  def clean(self, value):
    """Return value"""
    value = self.to_python(value)
    return value


# Widgets login form
class LoginForm(AuthenticationForm):
  """Widgets login authentication."""
  title_form = _('Please login')
  """title_form (string) : text of title box login """
  page_title = _('Login')
  """page_title (string) : text of title windows """
  bad_connect = _('Your username and password didn\'t match. Please try again.')
  """bad_connect (string) : text or bad connect """
  bad_access = _('Your account doesn\'t have access to this page. To proceed, please login with an account that access.')
  """bad_access (string) : text of bad access """
  not_connected = _('Please login to see this page.')
  """not_connected (string) : text of unconnect access """
  username = forms.CharField(label=_('Username'), max_length=30, widget=forms.TextInput(attrs={'class': 'nickname', 'title': _('Please enter your user name'), 'placeholder': _('Your user name')}))
  """username (CharField object) : widget of user name login """
  password = forms.CharField(label=_('Password'), max_length=30, widget=forms.PasswordInput(attrs={'class': 'password', 'title': _('Please enter your password'), 'placeholder': _('Your password')}))
  """password (CharField object) : widget of password user """
  button_userconnect = ButtonField(label='ButtonLogin', initial=_('Login'), required=False, widget=SubmitButton(attrs={'class': 'buttonlogin', 'title': _('Clic for login'), 'value': _('Login')}))
  """button_userconnect (ButtonFiel object) : widget of button login """
