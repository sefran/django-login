import os
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.core.files.images import get_image_dimensions
from django.utils.safestring import mark_safe
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _


### Create your models here.
# Validate image dimension
@deconstructible
class ImageDimensionsValidator(object):
  """ImageField dimensions validator"""
  def __init__(self, width=None, height=None, min_width=None, max_width=None, min_height=None, max_height=None):
    """
    Constructor
    
    Args:
        width (int): exact width
        height (int): exact height
        min_width (int): minimum width
        min_height (int): minimum height
        max_width (int): maximum width
        max_height (int): maximum height
    """
    self.width = width
    self.height = height
    self.min_width = min_width
    self.min_height = min_height
    self.max_width = max_width
    self.max_height = max_height
  def __call__(self, value):
    """Test sizes restrictions"""
    w, h = get_image_dimensions(value)
    if self.width is not None and w != self.width:
      raise ValidationError(_('%s width must be %dpx and not %dpx.')%(value.field.verbose_name.title(), self.width, w, ))
    if self.height is not None and h != self.height:
      raise ValidationError(_('%s height must be %dpx and not %dpx.') % (value.field.verbose_name.title(), self.height, h, ))
    if self.min_width is not None and w < self.min_width:
      raise ValidationError(_('%s minimum width must be %dpx and not %dpx.') % (value.field.verbose_name.title(), self.min_width, w, ))
    if self.min_height is not None and h < self.min_height:
      raise ValidationError(_('%s minimum height must be %dpx and not %dpx.') % (value.field.verbose_name.title(), self.min_height, h, ))
    if self.max_width is not None and w > self.max_width:
      raise ValidationError(_('%s maximum width must be %dpx and not %dpx.') % (value.field.verbose_name.title(), self.max_width, w, ))
    if self.max_height is not None and h > self.max_height:
      raise ValidationError(_('%s maximum height must be %dpx and not %dpx.') % (value.field.verbose_name.title(), self.max_height, h, ))
    
# Rename uploaded file
@deconstructible
class path_and_rename_file(object):
  """Replace same file saved validator"""
  def __init__(self, path, namefile):
    """
    Constructor
    
    Args:
        path (string): path to save
        namefile (string): file name
    """
    self.sub_path = path
    self.sub_name = namefile
  def __call__(self, instance, filename):
    """Replace saved file with downloaded file if exist"""
    #extract extension
    ext = filename.split('.')[-1]
    # create base name file
    filename = '{}.{}'.format(self.sub_name, ext)
    # create path file name
    fullname = os.path.join(self.sub_path, filename)
    # Remove exist file
    if os.path.exists(fullname):
      # delete same file name exist
      os.remove(fullname)
    return fullname

# Wallpaper login screen
class Wallpaper(models.Model):
  """Gesture of wallpapers from login screen"""
  # Fields
  #field_name = models.CharField(max_length=20, help_text='Gesture of wallpapers for login screen')
  
  # Metadata
  class Meta:
    """Name of item objects"""
    #ordering = ['field_name']
    verbose_name = _('Pictures of background')
    verbose_name_plural = _('Pictures of background')
  
  # Methods
  def get_adsolute_url(self):
    """Return the url to access a particular instance of Wallpaper."""
    return reverse('wallpapers-detail-view', args=[str(self.id)])
  
  # Methods of previews pictures
  def picture1_tag(self):
    """Preview of 1st wallpaper"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.picture1.name[5:]))
  def picture2_tag(self):
    """Preview of 2nd wallpaper"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.picture2.name[5:]))
  def picture3_tag(self):
    """Preview of 3rd wallpaper"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.picture3.name[5:]))
  def picture4_tag(self):
    """Preview of 4th wallpaper"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.picture4.name[5:]))
  def picture5_tag(self):
    """Preview of 5th wallpaper"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.picture5.name[5:]))

  # Admin widgets of wallpapers
  # picture 1
  picture1 = models.FileField(upload_to=path_and_rename_file('login/static/img/wallpaper', 'connexion0'), default='login/static/img/wallpaper/connexion0.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  picture1_tag.short_description = _('Preview image')
  # picture 2
  picture2 = models.FileField(upload_to=path_and_rename_file('login/static/img/wallpaper', 'connexion1'), default='login/static/img/wallpaper/connexion1.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  picture2_tag.short_description = _('Preview image')
  # picture 3
  picture3 = models.FileField(upload_to=path_and_rename_file('login/static/img/wallpaper', 'connexion2'), default='login/static/img/wallpaper/connexion2.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  picture3_tag.short_description = _('Preview image')
  # picture 4
  picture4 = models.FileField(upload_to=path_and_rename_file('login/static/img/wallpaper', 'connexion3'), default='login/static/img/wallpaper/connexion3.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  picture4_tag.short_description = _('Preview image')
  # picture 5
  picture5 = models.FileField(upload_to=path_and_rename_file('login/static/img/wallpaper', 'connexion4'), default='login/static/img/wallpaper/connexion4.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  picture5_tag.short_description = _('Preview image')

  # Name of created object
  def __str__(self):
    """Srting for representing the created object name."""
    #return self.field_name
    return 'wallpapers'

# Logo login screen
class Logo(models.Model):
  """Gesture of picture logo from login screen"""
  # Fields
  #field_name = models.CharField(max_length=20, help_text='Gesture of wallpapers for login screen')
  
  # Metadata
  class Meta:
    """Name of item objects"""
    verbose_name = _('Picture of logo')
    verbose_name_plural = _('Picture of logo')

  # Methods
  def get_adsolute_url(self):
    """Return the url to access a particular instance of Logo."""
    return reverse('logo-detail-view', args=[str(self.id)])
  
  # Methods of preview logo
  def logo_tag(self):
    """Preview of logo"""
    return mark_safe('<img src="%s" width="150" height="100" />' % (self.logo.name[5:]))

  # admin widgets of logo
  # logo picture
  logo = models.FileField(upload_to=path_and_rename_file('login/static/img', 'logo'), default='login/static/img/logo.svg', validators=[FileExtensionValidator(allowed_extensions=['svg'])])
  # Preview image
  logo_tag.short_description = _('Preview image')

  # Name of created object
  def __str__(self):
    """Srting for representing the created object name."""
    #return self.field_name
    return 'logo'

# favicon login screen
class Favicon(models.Model):
  """Gesture of picture favicon from login screen"""
  # Fields
  #field_name = models.CharField(max_length=20, help_text='Gesture of wallpapers for login screen')
  
  # Metadata
  class Meta:
    """Name of item objects"""
    verbose_name = _('Picture of favicon')
    verbose_name_plural = _('Picture of favicon')

  # Methods
  def get_adsolute_url(self):
    """Return the url to access a particular instance of Favicon."""
    return reverse('favicon-detail-view', args=[str(self.id)])
  
  # Methods of previews favicon
  def favicon_tag(self):
    """Preview of favicon"""
    return mark_safe('<img src="%s" width="64" height="64" />' % (self.favicon.name[5:]))

  # Admin widgets of favicon
  # favicon picture
  favicon = models.ImageField(upload_to=path_and_rename_file('login/static/img', 'favicon'), default='login/static/img/favicon.png', validators=[FileExtensionValidator(allowed_extensions=['png'])])
  # Preview image
  favicon_tag.short_description = _('Preview image')

  # Name of created object
  def __str__(self):
    """Srting for representing the created object name."""
    #return self.field_name
    return 'favicon'

# Picture lock and user login screen
class PasswordUser(models.Model):
  """Gesture of pictures password and user from login screen"""
  # Fields
  #field_name = models.CharField(max_length=20, help_text='Gesture of wallpapers for login screen')
  
  # Metadata
  class Meta:
    """Name of item objects"""
    verbose_name = _('Picture of icon password and icon user')
    verbose_name_plural = _('Picture of icon password and icon user')

  # Methods
  def get_adsolute_url(self):
    """Return the url to access a particular instance of PasswordUser."""
    return reverse('passworduser-detail-view', args=[str(self.id)])
  
  # Methods of previews pictures
  def passwordpicture_tag(self):
    """Preview of password lock picture"""
    return mark_safe('<img src="%s" width="64" height="320" />' % (self.passwordpicture.name[5:]))
  def userpicture_tag(self):
    """Preview of user login picture"""
    return mark_safe('<img src="%s" width="64" height="320" />' % (self.userpicture.name[5:]))
  # Admin widgets of password and user login pictures
  # password picture
  passwordpicture = models.ImageField(upload_to=path_and_rename_file('login/static/img', 'password'), default='login/static/img/password.png', validators=[FileExtensionValidator(allowed_extensions=['png']), ImageDimensionsValidator(width=32, height=160)])
  # Preview image
  passwordpicture_tag.short_description = _('Preview image')
  # user picture
  userpicture = models.ImageField(upload_to=path_and_rename_file('login/static/img', 'user'), default='login/static/img/user.png', validators=[FileExtensionValidator(allowed_extensions=['png']), ImageDimensionsValidator(width=32, height=160)])
  # Preview image
  userpicture_tag.short_description = _('Preview image')

  # Name of created object
  def __str__(self):
    """Srting for representing the created object name."""
    #return self.field_name
    return 'passworduserpictures'
