from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

# Register your models here.
from . import models, forms


# Modify admin login view to plugin login view
admin.site.login_form = forms.LoginForm
admin.site.login_template = 'login/login.html'
admin.autodiscover()

# add admin view Wallpaper
@admin.register(models.Wallpaper)
class AdminWallpaperPictures(admin.ModelAdmin):
  """Admin view wallpaper login screen"""
  def image_tag(self, obj):
    """Create html preview logo picture for group's wallpapers"""
    return format_html('<img width="400" height="300" src="/static/img/wallpapers-login.svg" />')
    image_tag.short_description = _('Image preview')
  # Add admin widget logo image wallpapers.
  list_display = ['image_tag',]
  # Add widgets wallpapers with widgets previews wallpaper to admin view wallpapers login screen.
  fields = ['picture1', 'picture1_tag', 'picture2', 'picture2_tag', 'picture3', 'picture3_tag', 'picture4', 'picture4_tag', 'picture5', 'picture5_tag']
  readonly_fields = ['picture1_tag', 'picture2_tag', 'picture3_tag', 'picture4_tag', 'picture5_tag']
  def has_add_permission(self, request):
    """Create restriction for only one object Wallpaper"""
    num_objects = self.model.objects.count()
    # Only one instance of Wallpaper objects
    if num_objects >= 1:
      return False
    else:
      return True

# add admin view Logo
@admin.register(models.Logo)
class AdminLogoPicture(admin.ModelAdmin):
  """Admin view logo login screen"""
  def image_tag(self, obj):
    """Create html preview logo login screen"""
    return format_html('<img width="400" height="300" src="/static/img/logo.svg" />')
    image_tag.short_description = _('Image preview')
  # Add admin widget logo preview.
  list_display = ['image_tag',]
  # Add widget logo and previews logo to admin view logo.
  fields = ['logo', 'logo_tag']
  readonly_fields = ['logo_tag']
  def has_add_permission(self, request):
    """Create restriction for only one object Logo"""
    num_objects = self.model.objects.count()
    # Only one instance of Logo objects.
    if num_objects >= 1:
      return False
    else:
      return True


# add admin view Favicon
@admin.register(models.Favicon)
class AdminFaviconPicture(admin.ModelAdmin):
  """Admin view favicon login screen"""
  def image_tag(self, obj):
    """Create html preview favicon login screen"""
    return format_html('<img width="64" height="64" src="/static/img/favicon.png" />')
    image_tag.short_description = _('Image preview')
  # Add widget image favicon preview.
  list_display = ['image_tag',]
  # Add widget favicon and previews favicon to admin view favicon.
  fields = ['favicon', 'favicon_tag']
  readonly_fields = ['favicon_tag']
  def has_add_permission(self, request):
    """Create restriction for only one object Favicon"""
    num_objects = self.model.objects.count()
    # Only one instance of Favicon objects.
    if num_objects >= 1:
      return False
    else:
      return True

# add admin view PasswordUser
@admin.register(models.PasswordUser)
class AdminPasswordUserPicture(admin.ModelAdmin):
  """Admin view password lock and user picture login screen"""
  def image_tag(self, obj):
    """Create html preview logo picture for password lock and user picture login screen"""
    return format_html('<img width="128" height="128" src="/static/img/passworduser-login.svg" />')
    image_tag.short_description = _('Image preview')
  # Add admin widget logo image password lock and user icon.
  list_display = ['image_tag',]
  # Add widgets logo image password lock, preview logo image password lock, logo user icon and preview logo user icon to admin view PasswordUser.
  fields = ['passwordpicture', 'passwordpicture_tag', 'userpicture', 'userpicture_tag']
  readonly_fields = ['passwordpicture_tag', 'userpicture_tag']
  def has_add_permission(self, request):
    """Create restriction for only one object PasswordUser"""
    num_objects = self.model.objects.count()
    # Only one instance of PasswordUser objects.
    if num_objects >= 1:
      return False
    else:
      return True
